#include "MainWindow.h"
#include <QGridLayout>
#include <QPushButton>
#include <UI/UIModel.h>
#include "UI/UIView.h"
#include "QLabel"

MainWindow::MainWindow() {
    setWindowTitle("Planning Using State-Time Space");

    float sceneWidth = 600, sceneHeight = 600;

    QLabel *label = new QLabel("Click New Goal Positions to generate goal "
                               "positions for mice, then click run to plan paths"
                               " for these mice. A* is used to find collision-free"
                               " trajectories in the state-time space for these mice.");
    label->setWordWrap(true);
    label->setFixedWidth(sceneWidth);

    QPushButton *newGoalPos = new QPushButton(this);
    newGoalPos->setText("New Goal Positions");
    QPushButton *run = new QPushButton(this);
    run->setText("Run");

    QHBoxLayout *hBoxLayout = new QHBoxLayout();
    hBoxLayout->addWidget(newGoalPos);
    hBoxLayout->addWidget(run);

    UIModel *model = new UIModel(this, sceneWidth, sceneHeight);
    UIView *viewer = new UIView(model, this);

    QGridLayout *layout = new QGridLayout();
    layout->addWidget(label, 0, 0);
    layout->addLayout(hBoxLayout, 1, 0);
    layout->addWidget(viewer, 2, 0, 5, 5);

    QWidget *centralWidget = new QWidget(this);
    centralWidget->setLayout(layout);
    this->setCentralWidget(centralWidget);

    //  make the buttons do things
    connect(newGoalPos, SIGNAL(clicked()), model, SLOT(slot_newGoalPos()));
    connect(run, SIGNAL(clicked()), model, SLOT(slot_run()));
}

MainWindow::~MainWindow() {

}
