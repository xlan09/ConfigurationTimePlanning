#-------------------------------------------------
#
# Project created by QtCreator 2016-07-24T09:25:07
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = StateTimePlanning
TEMPLATE = app


SOURCES += main.cpp\
        MainWindow.cpp \
    UI/UIView.cpp \
    Core/RoadMap.cpp \
    Core/Node.cpp \
    UI/UIModel.cpp \
    Core/ConfigurationTimeGrid.cpp \
    UI/Mouse.cpp \
    Core/SearchUtil.cpp \
    UI/QNoPathFoundWindow.cpp

HEADERS  += MainWindow.h \
    UI/UIView.h \
    Core/RoadMap.h \
    Core/Node.h \
    UI/UIModel.h \
    Core/ConfigurationTimeGrid.h \
    UI/Mouse.h \
    Core/SearchUtil.h \
    UI/QNoPathFoundWindow.h

CONFIG += c++11
