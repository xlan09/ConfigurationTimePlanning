#include "QNoPathFoundWindow.h"
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>

QNoPathFoundWindow::QNoPathFoundWindow(QWidget *parent) : QWidget(parent)
{
    setStyleSheet("QNoPathFoundWindow { background: rgb(237,224,200); }");
    setFixedSize(425,205);
    QVBoxLayout *layout = new QVBoxLayout(this);

    QLabel* msg = new QLabel("No path found, will regenerate goal positions and try again!", this);
    msg->setStyleSheet("QLabel { color: rgb(119,110,101); font: 14pt; font: bold;} ");
    msg->setWordWrap(true);

    m_ok = new QPushButton("OK?", this);
    m_ok->setFixedHeight(50);
    m_ok->setFixedWidth(100);

    layout->insertWidget(0, msg, 0, Qt::AlignCenter);
    layout->insertWidget(1, m_ok,0, Qt::AlignCenter);
}

QPushButton *QNoPathFoundWindow::okButton() const
{
    return m_ok;
}

