#include "UIModel.h"
#include "Core/Node.h"
#include <QGraphicsLineItem>
#include <QGraphicsEllipseItem>
#include <QGraphicsItemAnimation>
#include <math.h>
#include <stdlib.h> // srand
#include <stdexcept>
#include <utility>
#include <Core/ConfigurationTimeGrid.h>
#include "UI/Mouse.h"
#include <stdexcept>
#include <algorithm>
#include <QTimeLine>
#include <stdexcept>
#include <QPushButton>

static const float GoalTokenRadius = 15;

void UIModel::slot_newGoalPos()
{
    try
    {
        m_noPathFoundWindow.hide();

        m_goalPositionsInRoadMap = m_roadmap.getNewGoalPos();
        for(size_t i = 0; i < m_goalPositionsInRoadMap.size(); ++i)
        {
            m_goalPos[i]->setRect(m_goalPositionsInRoadMap[i]->pos().first, m_goalPositionsInRoadMap[i]->pos().second, GoalTokenRadius, GoalTokenRadius);
        }
    }
    catch(const std::exception &exp)
    {
        std::cout << "Exceptions thrown " << exp.what() << std::endl;
    }
}

void UIModel::slot_run()
{
    try
    {
        std::vector<std::vector<Node *>> paths;
        if(m_roadmap.plan(paths))
        {
            advanceMice(paths);
            // after mouse reached goal, update its positions
            m_roadmap.updateMousePos(m_goalPositionsInRoadMap);
        }
        else
        {
            m_noPathFoundWindow.show();
        }
    }
    catch(const std::exception &exp)
    {
        std::cout << "Exceptions thrown " << exp.what() << std::endl;
    }
}

UIModel::UIModel(QObject *parent, float width, float height) : QGraphicsScene(parent)
{
    m_sceneSize = std::make_pair(width, height);
    setSceneRect(0, 0, m_sceneSize.first, m_sceneSize.second);

    // draw scene boundary
    QLineF topLine(sceneRect().topLeft(),
                   sceneRect().topRight());
    QLineF leftLine(sceneRect().topLeft(),
                    sceneRect().bottomLeft());
    QLineF rightLine(sceneRect().topRight(),
                     sceneRect().bottomRight());
    QLineF bottomLine(sceneRect().bottomLeft(),
                      sceneRect().bottomRight());

    QPen myPen = QPen(Qt::red);

    addLine(topLine, myPen);
    addLine(leftLine, myPen);
    addLine(rightLine, myPen);
    addLine(bottomLine, myPen);

    m_roadmap = RoadMap(m_sceneSize, 36);

    drawGraphDFS(m_roadmap.getRoot());
    // initialize mouse on nodes of roadmap, must be placed after roadmap is constructed
    initializeMouse();
    initializeGoalPos();
    initializeAnimations();

    connect(m_noPathFoundWindow.okButton(), SIGNAL(clicked()), this, SLOT(slot_newGoalPos()));
}

/**
 * @brief UIModel::drawGraph
 * Used for debugging
 * @param nodes
 */
void UIModel::drawGraph(const std::vector<std::shared_ptr<Node> > &nodes)
{
    for(size_t i = 0; i < nodes.size(); ++i)
    {
        drawNode(nodes[i].get());

        std::vector<Node *> neighbors = nodes[i]->neighbors();
        for(size_t j = 0; j < neighbors.size(); ++j)
        {
            drawLine(nodes[i].get(), neighbors[j]);
        }
    }
}

void UIModel::drawGraphDFS(Node *root)
{
    if (root == nullptr) return;
    std::unordered_set<Node *> visited;
    drawGraphDFSHelper(root, visited);
}

void UIModel::drawGraphDFSHelper(Node *currNode, std::unordered_set<Node *> &visited)
{
    if (currNode == nullptr) return;

    if (visited.find(currNode) == visited.end())
    {
        visited.insert(currNode); // mark it visited
        // draw this node
        drawNode(currNode);
        std::vector<Node *> neighbors = currNode->neighbors();
        for(unsigned int i = 0; i < neighbors.size(); ++i)
        {
            // draw line between neighbors
            drawLine(currNode, neighbors[i]);
            drawGraphDFSHelper(neighbors[i], visited);
        }
    }
}

void UIModel::drawNode(const Node *ptr)
{
    if (ptr == nullptr) return;

    float x = ptr->pos().first;
    float y = ptr->pos().second;
    QGraphicsEllipseItem *item  = addEllipse(x, y, 2, 2);
    item->setBrush(Qt::blue);
    addItem(item);
}

void UIModel::drawLine(const Node *node1, const Node *node2)
{
    if (node1 == nullptr || node2 == nullptr) return;

    QGraphicsLineItem *lineItem = addLine(node1->pos().first, node1->pos().second, node2->pos().first, node2->pos().second);
    QPen newPen(Qt::PenStyle::SolidLine);
    newPen.setColor(Qt::blue);
    newPen.setWidth(1);
    lineItem->setPen(newPen);
    addItem(lineItem);
}

void UIModel::initializeMouse()
{
    std::vector<Node *> randomMousePos = m_roadmap.getNewMousePos();

    for (size_t i = 0; i < randomMousePos.size(); ++i)
    {
        std::shared_ptr<Mouse> newMouse = std::shared_ptr<Mouse>(new Mouse(m_mice.size()));
        newMouse->setPos(randomMousePos[i]->pos().first, randomMousePos[i]->pos().second);
        addItem(newMouse.get());
        m_mice.push_back(newMouse);
    }
}

void UIModel::initializeGoalPos()
{
    m_goalPositionsInRoadMap = m_roadmap.getNewGoalPos();
    for (size_t i = 0; i < m_goalPositionsInRoadMap.size(); ++i)
    {
        std::shared_ptr<QGraphicsEllipseItem> newGoal = std::shared_ptr<QGraphicsEllipseItem>(new QGraphicsEllipseItem());
        newGoal->setRect(m_goalPositionsInRoadMap[i]->pos().first, m_goalPositionsInRoadMap[i]->pos().second, GoalTokenRadius, GoalTokenRadius);
        newGoal->setBrush(Qt::cyan);
        addItem(newGoal.get());
        m_goalPos.push_back(newGoal);
    }
}

void UIModel::initializeAnimations()
{
    m_miceAnimations.clear();
    for(size_t i = 0; i < m_mice.size(); ++i)
    {
        std::shared_ptr<QGraphicsItemAnimation> animation = std::shared_ptr<QGraphicsItemAnimation>(new QGraphicsItemAnimation());
        animation->setItem(m_mice[i].get());
        m_miceAnimations.push_back(animation);
    }
}

void UIModel::advanceMice(const std::vector<std::vector<Node *> > &paths)
{
    /**
     * @brief initializeAnimations
     * This is super important. If not have this, the animation is weird since
     * each time after we generate new goal positions, and rerun, the animation
     * starts from the original positions, not from the current mouse positions.
     * This is because the QGraphicsItemAnimation.clear() does not clear the
     * current TimeLine, but each time, we use a different timeline. To solve this issue,
     *  each time we reinitialize the animation objects.
     */
    initializeAnimations();

    // get the longest time
    size_t longestTimeSteps = 0;
    for(size_t i = 0; i < paths.size(); ++i)
    {
        longestTimeSteps = std::max(longestTimeSteps, paths[i].size());
        // clear the original path used for animation, number of paths should
        // be equal to number of mice animations
        m_miceAnimations[i]->clear();
    }

    int interval = 600; // unit is milliseconds
    std::vector<QTimeLine *> timers;
    for(size_t i = 0; i < paths.size(); ++i)
    {
        QTimeLine *timer = new QTimeLine();
        timer->setUpdateInterval(interval);
        timer->setDuration(paths[i].size() * interval);
        m_miceAnimations[i]->setTimeLine(timer);
        timers.push_back(timer);
    }

    // note last element in paths[j] is the startnode, first element is the end node
    for(int i = (int)longestTimeSteps - 1; i >= 0; --i)
    {
        for(size_t j = 0; j < paths.size(); ++j)
        {
            int pathSize = paths[j].size();
            if (i < pathSize)
            {
                m_miceAnimations[j]->setPosAt(1.0 - i / ((float) pathSize), QPointF(paths[j][i]->pos().first, paths[j][i]->pos().second));
            }
        }
    }

    for(size_t i = 0; i < timers.size(); ++i)
    {
        timers[i]->start();
    }
}
