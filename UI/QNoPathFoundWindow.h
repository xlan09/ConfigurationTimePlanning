#ifndef QNOPATHFOUNDWINDOW_H
#define QNOPATHFOUNDWINDOW_H

#include <QWidget>

class QPushButton;

class QNoPathFoundWindow : public QWidget
{
    Q_OBJECT
public:
    explicit QNoPathFoundWindow(QWidget *parent = 0);
    QPushButton *okButton() const;

private:
    QPushButton *m_ok;
};

#endif // QNOPATHFOUNDWINDOW_H
