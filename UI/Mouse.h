#ifndef MOUSE_H
#define MOUSE_H

#include <QGraphicsItem>

typedef std::pair<float, float> Point;

/**
 * @brief The Mouse class
 * This class is from Qt example: Colliding Mice Example
 */
class Mouse : public QGraphicsItem
{
public:
    Mouse(int mouseId);

    QRectF boundingRect() const Q_DECL_OVERRIDE;
    QPainterPath shape() const Q_DECL_OVERRIDE;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget) Q_DECL_OVERRIDE;

    int getId() const;

private:
    int id;
    qreal angle;
    qreal speed;
    qreal mouseEyeDirection;
    QColor color;
};

#endif
