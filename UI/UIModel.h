#ifndef UIMODEL_H
#define UIMODEL_H

#include <QGraphicsScene>
#include "Core/RoadMap.h"
#include <unordered_set>
#include "UI/QNoPathFoundWindow.h"

class QGraphicsItemAnimation;
class Mouse;

class UIModel : public QGraphicsScene {
    Q_OBJECT
  public slots:
    void slot_newGoalPos();
    void slot_run();

  public:
    UIModel(QObject *parent, float width, float height);

  private:
    std::pair<float, float> m_sceneSize;
    RoadMap m_roadmap;
    std::vector<std::shared_ptr<Mouse> > m_mice;
    std::vector<std::shared_ptr<QGraphicsEllipseItem> > m_goalPos;
    std::vector<std::shared_ptr<QGraphicsItemAnimation> > m_miceAnimations;
    std::vector<Node *> m_goalPositionsInRoadMap;
    QNoPathFoundWindow m_noPathFoundWindow;

    void drawGraphDFS(Node *root);
    void drawGraphDFSHelper(Node *currNode, std::unordered_set<Node *> &visited);
    void drawGraph(const std::vector<std::shared_ptr<Node> > &nodes);
    void drawNode(const Node *ptr);
    void drawLine(const Node *node1, const Node *node2);
    void initializeMouse();
    void initializeGoalPos();
    void initializeAnimations();
    void advanceMice(const std::vector<std::vector<Node *> > &paths);
};

#endif // UIMODEL_H
