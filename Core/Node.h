#ifndef NODE_H
#define NODE_H
#include <utility>
#include <vector>
#include <iostream>

typedef std::pair<float, float> Point;
typedef std::pair<Point, float> State;
typedef std::pair<int, int> Edge; // edge from nodeId to another nodeId

class Node
{
public:
    Node();
    Node(int id, float x, float y);
    Node(int id, const Point &point);

    friend std::ostream &operator <<(std::ostream &os, const Node *node);
    float dist2node(const Node *node) const;
    State state() const;
    std::vector<Node *> neighbors() const;
    void addNeighbor(Node *node);
    std::pair<float, float> pos() const;

    int id() const;

private:
    int m_id;
    State m_state;
    std::vector<Node *> m_neighbors;
};

Point &operator +=(Point &arg1, const Point &arg2);
Point operator +(const Point &arg1, const Point &arg2);
Point &operator -=(Point &arg1, const Point &arg2);
Point operator -(const Point &arg1, const Point &arg2);
Point &operator *=(Point &arg, float num);
Point operator *(const Point &arg, float num);
Point operator *(float num, const Point &arg);
Point &operator /=(Point &arg, float num);
Point operator /(const Point &arg, float num);
Point operator /(float num, const Point &arg);
bool operator ==(const Point &point1, const Point &point2);
bool operator !=(const Point &point1, const Point &point2);
std::ostream &operator <<(std::ostream &os, const Point &point);
Point normalizeVec(const Point &arg);

// in case of using Point as key of hashmap
namespace std
{
    template <>
    struct hash<Point>
    {
        size_t operator()(const Point &k) const
        {
            // Compute individual hash values for two data members and combine them using XOR and bit shifting
            return ((hash<float>()(k.first) ^ (hash<float>()(k.second) << 1)) >> 1);
        }
    };

    template <>
    struct hash<Edge>
    {
        size_t operator()(const Edge &k) const
        {
            // Compute individual hash values for two data members and combine them using XOR and bit shifting
            return ((hash<int>()(k.first) ^ (hash<int>()(k.second) << 1)) >> 1);
        }
    };
}

#endif // NODE_H
