#ifndef SEARCHUTIL_H
#define SEARCHUTIL_H

#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>
#include <iostream>

class Node;
typedef std::pair<int, int> Edge;

class SearchUtil
{
public:
    SearchUtil();
    static Node *findUnvisitedNodeWithShortestDist(const std::unordered_map<Node *, std::pair<Node *, float> > &startNodeToAllNodesDist, const std::unordered_set<Node *> &unVisited);

    static void findPath(const std::unordered_map<Node *, std::pair<Node *, float> > &startNodeToAllNodesDist, Node *endNode, std::vector<Node *> &path);

    static bool isReserved(Node * node,
                           Node *neighbor,
                           Node *goalNode,
                           float timeToCurrNode,
                           float timeAtNeighbor,
                           const std::unordered_map<Edge, std::unordered_set<float> > &reserved,
                           const std::unordered_map<Node *, float> &node2reservedStartingTime);

    static std::vector<float> range(float start, float end, float interval);

    static void randomPlanningOrder(std::vector<int> &planningOrder);

    template<typename T1, typename T2>
    static void output(const std::unordered_map<T1 *, T2> &dict)
    {
        for(typename std::unordered_map<T1 *, T2>::const_iterator iter = dict.begin(); iter != dict.end(); ++iter)
        {
            std::cout << "key: " << iter->first->pos() << ", value: " << iter->second << std::endl;
        }
    }

    template<typename T1, typename T2>
    static void output(const std::unordered_map<T1 *, std::unordered_set<T2> > &dict)
    {
        for(typename std::unordered_map<T1 *, std::unordered_set<T2> >::const_iterator iter = dict.begin(); iter != dict.end(); ++iter)
        {
            std::cout << "key: " << iter->first->pos() << ", values: ";

            std::unordered_set<T2> values = iter->second;
            for(typename std::unordered_set<T2>::const_iterator iter2 = values.begin(); iter2 != values.end(); ++iter2)
            {
                std::cout << *iter2 << ", ";
            }

            std::cout << std::endl;
        }
    }
};

#endif // SEARCHUTIL_H
