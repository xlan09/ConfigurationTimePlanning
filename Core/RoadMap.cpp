#include "RoadMap.h"
#include <unordered_set>
#include "Core/Node.h"
#include <stack>
#include <math.h>
#include <cstdlib> // srand and rand
#include <time.h>
#include "Core/SearchUtil.h"
#include <algorithm>
#include <map>

static const int MouseCount = 15;

RoadMap::RoadMap()
{

}

RoadMap::RoadMap(const std::pair<float, float> &sceneSize, size_t numNodes)
{
    generateMap(sceneSize, numNodes);
    m_stateTimeGrid = ConfigurationTimeGrid(getRoot(), 10, 2);
}

bool RoadMap::plan(std::vector<std::vector<Node *> > &paths) const
{
    std::vector<int> planningOrder;
    for(size_t i = 0; i < m_mousePos.size(); ++i)
    {
        planningOrder.push_back(i);
    }

    bool planningSuccess = false;

    std::cout << "*********************Start Planning****************" << std::endl;

    int totalCycleCount = 10;
    for(int cycleCount = 1; cycleCount <= totalCycleCount; ++cycleCount)
    {
        std::cout << "-------------------Planning cycle " << cycleCount << " ----------------" << std::endl;
        paths.clear();
        bool allPathsFound = true;

        std::unordered_map<Edge, std::unordered_set<float> > reserved;

        /**
         * @brief node2reservedStartingTime
         * Key is node, value is the starting time after which the node is always reserved
         * This is used to deal with some mice reached their goals before others.
         * In this case, we should view these mice as static obstacles.
         */
        std::unordered_map<Node *, float> node2reservedStartingTime;

        for(size_t i = 0; i < planningOrder.size(); ++i)
        {
            std::vector<Node *> path;
            if(m_stateTimeGrid.searchPathByAstar(m_mousePos[planningOrder[i]], m_goalPos[planningOrder[i]], path, reserved, node2reservedStartingTime) == false)
            {
                allPathsFound = false;
                break;
            }

            /**
             * Output path for debugging
             */
            std::cout << "--------Planned path------------" << std::endl;
            outputPath(path);

            paths.push_back(path);
            updateReservedNodes(reserved, path, node2reservedStartingTime);
        }

        if (allPathsFound)
        {
            planningSuccess = true;
            break;
        }
        else
        {
            std::random_shuffle(planningOrder.begin(), planningOrder.end());
        }
    }

    if (planningSuccess == false)
    {
        paths.clear(); // returns no path
    }

    return planningSuccess;
}

unsigned int RoadMap::getNumNodes() const
{
    if (m_nodes.empty()) return 0;
    else return m_nodes.size();
}

void RoadMap::getFirstNNodes(size_t n, std::vector<Node *> &firstNnodes) const
{
    firstNnodes.clear();
    if (getNumNodes() < n)
    {
        throw std::invalid_argument("Number of nodes in roadmap " + std::to_string(getNumNodes()) + " is smaller than requested number of nodes: " + std::to_string(n));
    }

    std::stack<Node *> nodes;
    std::unordered_set<Node *> visited;
    nodes.push(getRoot());
    while(!nodes.empty() && n > 0)
    {
        Node *curr = nodes.top();
        nodes.pop();
        if(visited.find(curr) == visited.end())
        {
            n--;
            firstNnodes.push_back(curr);
            visited.insert(curr);
            std::vector<Node *> neighbors = curr->neighbors();
            for(size_t i = 0; i < neighbors.size(); ++i)
            {
                if(visited.find(neighbors[i]) == visited.end())
                {
                    nodes.push(neighbors[i]);
                }
            }
        }
    }
}

void RoadMap::generateFreePos(size_t numPos, std::vector<Node *> &freePos) const
{
    srand(time(nullptr));
    freePos.clear();
    size_t numNodes = getNumNodes();

    for(size_t i = 0; i < numPos; ++i)
    {
        int pos = 0;
        while(1)
        {
            pos = rand() % numNodes;
            if((!isCollideWithMouse(m_nodes[pos].get()))  && (!isGoalPos(m_nodes[pos].get())))
            {
                break;
            }
        }

        freePos.push_back(m_nodes[pos].get());
    }
}

Node *RoadMap::getRoot() const
{
    if (m_nodes.empty()) return nullptr;
    else return m_nodes[0].get();
}

std::vector<Node *> RoadMap::getNewMousePos()
{
    if (MouseCount * 2 >= m_nodes.size())
    {
        throw std::invalid_argument("Too many mice!");
    }

    generateFreePos(MouseCount, m_mousePos);

    return m_mousePos;
}

std::vector<Node *> RoadMap::getNewGoalPos()
{
    if (MouseCount * 2 >= m_nodes.size())
    {
        throw std::invalid_argument("Too many goals caused by too many mice!");
    }

    generateFreePos(MouseCount, m_goalPos);

    return m_goalPos;
}

std::vector<Node *> RoadMap::getCurrGoalPos() const
{
    return m_goalPos;
}

void RoadMap::updateMousePos(const std::vector<Node *> &newPos)
{
    m_mousePos = newPos;
}

std::vector<Node *> RoadMap::getCurrMousePos() const
{
    return m_mousePos;
}

bool RoadMap::isCollideWithMouse(const Node *node) const
{
    bool res = false;
    for(size_t i = 0; i < m_mousePos.size(); ++i)
    {
        if (node->pos() == m_mousePos[i]->pos())
        {
            res = true;
            break;
        }
    }

    return res;
}

bool RoadMap::isGoalPos(const Node *node) const
{
    bool res = false;
    for(size_t i = 0; i < m_goalPos.size(); ++i)
    {
        if (node->pos() == m_goalPos[i]->pos())
        {
            res = true;
            break;
        }
    }

    return res;
}

void RoadMap::generateMap(const std::pair<float, float> &sceneSize, size_t num)
{
    // clear nodes
    m_nodes.clear();

    float sceneNWCornerX = 0;
    float sceneNWCornerY = 0;
    float width = sceneSize.first;
    float height = sceneSize.second;

    // calculate number of nodes in each dimension
    float ratio = height / width;
    unsigned int numNodesInXDim = (unsigned int)sqrt(num / ratio);
    unsigned int numNodesInYDim = (unsigned int)(numNodesInXDim * ratio);

    if (numNodesInXDim == 0 || numNodesInYDim == 0) throw std::logic_error("num of nodes in one dimension can not be zero when generating graphs");

    float gridWidth = width / numNodesInXDim;
    float gridHeight = height / numNodesInYDim;
    for(unsigned int i = 0; i < numNodesInYDim; ++i)
    {
        for(unsigned int j = 0; j < numNodesInXDim; ++j)
        {
            float randX = sceneNWCornerX + gridWidth * j + gridWidth * (((float)rand()) / RAND_MAX);
            float randY = sceneNWCornerY + gridHeight * i + gridHeight * (((float)rand()) / RAND_MAX);

            std::shared_ptr<Node> temp = std::shared_ptr<Node>(new Node(m_nodes.size(), randX, randY));
            if(j != 0) makeNeighbors(m_nodes[numNodesInXDim * i + j - 1].get(), temp.get());
            if (i != 0) makeNeighbors(m_nodes[numNodesInXDim * (i - 1) + j].get(), temp.get());

            m_nodes.push_back(temp);
        }
    }
}

void RoadMap::makeNeighbors(Node *node1, Node *node2) const
{
    node1->addNeighbor(node2);
    node2->addNeighbor(node1);
}

void RoadMap::updateReservedNodes(std::unordered_map<Edge, std::unordered_set<float> > &reserved, const std::vector<Node *> &path, std::unordered_map<Node *, float> &node2reservedStartingTime) const
{
    // update the reserved nodes based on path
    float travelTimeInNodes = m_stateTimeGrid.getTravelTimeBetweeNodes();
    float timeOnPath = 0;
    for(int i = path.size() - 1; i > 0; --i)
    {
        Edge forwardEdge = std::make_pair(path[i]->id(), path[i - 1]->id());
        Edge backwardEdge = std::make_pair(path[i - 1]->id(), path[i]->id());

        if (reserved.find(forwardEdge) == reserved.end())
        {
            reserved[forwardEdge] = std::unordered_set<float>();
        }

        reserved[forwardEdge].insert(timeOnPath);
        reserved[forwardEdge].insert(timeOnPath + travelTimeInNodes);

        if (reserved.find(backwardEdge) == reserved.end())
        {
            reserved[backwardEdge] = std::unordered_set<float>();
        }

        reserved[backwardEdge].insert(timeOnPath);
        reserved[backwardEdge].insert(timeOnPath + travelTimeInNodes);

        timeOnPath += travelTimeInNodes;
    }

    // path front is the goal node
    node2reservedStartingTime[path.front()] = travelTimeInNodes * (path.size() - 1);
}

void RoadMap::outputPath(const std::vector<Node *> &path) const
{
    float reservedTime = 0;
    float travelTimeInNodes = m_stateTimeGrid.getTravelTimeBetweeNodes();
    for(std::vector<Node *>::const_reverse_iterator iter = path.rbegin(); iter != path.rend(); ++iter)
    {
        std::cout << (*iter)->pos() << ", time: " << reservedTime << std::endl;
        reservedTime += travelTimeInNodes;
    }
}
