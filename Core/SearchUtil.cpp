#include "SearchUtil.h"
#include <limits>
#include <math.h>
#include "Core/Node.h"

SearchUtil::SearchUtil()
{

}

Node *SearchUtil::findUnvisitedNodeWithShortestDist(const std::unordered_map<Node *, std::pair<Node *, float> > &startNodeToAllNodesDist, const std::unordered_set<Node *> &unVisited)
{
    Node *nodeWithMinDist = nullptr;
    float minDist = std::numeric_limits<float>::max();

    for(std::unordered_set<Node *>::const_iterator iter = unVisited.begin(); iter != unVisited.end(); ++iter)
    {
        if (startNodeToAllNodesDist.at(*iter).second < minDist)
        {
            minDist = startNodeToAllNodesDist.at(*iter).second;
            nodeWithMinDist = *iter;
        }
    }

    return nodeWithMinDist;
}

void SearchUtil::findPath(const std::unordered_map<Node *, std::pair<Node *, float> > &startNodeToAllNodesDist, Node *endNode, std::vector<Node *> &path)
{
    path.clear();

    Node *currNode = endNode;
    while (currNode != nullptr)
    {
        path.push_back(currNode);
        currNode = startNodeToAllNodesDist.at(currNode).first;
    }
}

bool SearchUtil::isReserved(Node *node,
                            Node *neighbor,
                            Node *goalNode,
                            float timeToCurrNode,
                            float timeAtNeighbor,
                            const std::unordered_map<Edge, std::unordered_set<float> > &reserved,
                            const std::unordered_map<Node *, float> &node2reservedStartingTime)
{
    bool res = false;
    Edge currEdge = std::make_pair(node->id(), neighbor->id());
    if ((reserved.find(currEdge) != reserved.end() \
            && (reserved.at(currEdge).find(timeToCurrNode) != reserved.at(currEdge).end() || reserved.at(currEdge).find(timeAtNeighbor) != reserved.at(currEdge).end()) ) \
            || (node2reservedStartingTime.find(neighbor) != node2reservedStartingTime.end() && timeAtNeighbor >= node2reservedStartingTime.at(neighbor))) // the last condition is to check some mice reached goals earlier and stopped there as a static obstacle
    {
        res = true;
    }

    // If we reach goal at time t and the mouse stays at there, then we have to check whether other mice will pass here after t.
    // This is caused by the planning order of the paths.
    // Since we may plan a path(this path is planned before the current path) passing this goal position at time t1(t1 > t)
    if (neighbor == goalNode)
    {
        std::vector<Node *> goalNodeNeighbors = goalNode->neighbors();
        for(size_t i =  0; i < goalNodeNeighbors.size(); ++i)
        {
            std::vector<Edge> edgesToCheck;
            Edge edgeToGoalNode = std::make_pair(goalNodeNeighbors[i]->id(), goalNode->id());
            Edge edgeFromGoalNode = std::make_pair(goalNode->id(), goalNodeNeighbors[i]->id());
            edgesToCheck.push_back(edgeToGoalNode);
            edgesToCheck.push_back(edgeFromGoalNode);

            for(size_t k = 0; k < edgesToCheck.size(); ++k)
            {
                Edge currEdge = edgesToCheck[k];
                if (reserved.find(currEdge) != reserved.end())
                {
                    std::unordered_set<float> times = reserved.at(currEdge);
                    for(auto iter = times.begin(); iter != times.end(); ++iter)
                    {
                        // If we reached goal before other paths passing here
                        if (timeAtNeighbor <= (*iter))
                        {
                            res = true;
                            break;
                        }
                    }
                }

                if(res == true)
                {
                    break;
                }
            }

            if(res == true)
            {
                break;
            }
        }
    }

    return res;
}

std::vector<float> SearchUtil::range(float start, float end, float interval)
{
    float resolution = pow(10, -8);

    if (start > end || interval < resolution)
    {
        throw std::invalid_argument("End can not be smaller than start and interval must be positive!");
    }

    std::vector<float> res;

    if (fabs(end - start) < resolution)
    {
        res.push_back(start);
    }
    else if (fabs(end - start) <= interval)
    {
        res.push_back(start);
        res.push_back(end);
    }
    else
    {
        float curr = start;
        while(curr < end)
        {
            res.push_back(curr);
            curr += interval;
        }

        res.push_back(end);
    }

    return res;
}

void SearchUtil::randomPlanningOrder(std::vector<int> &planningOrder)
{
    for(size_t i = 0; i < planningOrder.size(); ++i)
    {

    }
}
