#include "Node.h"
#include <math.h>

Node::Node() : m_id(0), m_state(std::make_pair(std::make_pair(0, 0), 0))
{

}

Node::Node(int id, float x, float y) : m_id(id), m_state(std::make_pair(std::make_pair(x, y), 0))
{

}

Node::Node(int id, const Point &point) : m_id(id), m_state(std::make_pair(point, 0))
{

}

float Node::dist2node(const Node *node) const
{
    return sqrt(pow(node->state().first.first - m_state.first.first, 2) + pow(node->state().first.second - m_state.first.second, 2));
}

State Node::state() const
{
    return m_state;
}
std::vector<Node *> Node::neighbors() const
{
    return m_neighbors;
}

void Node::addNeighbor(Node *node)
{
    m_neighbors.push_back(node);
}

std::pair<float, float> Node::pos() const
{
    return m_state.first;
}
int Node::id() const
{
    return m_id;
}


std::ostream & operator <<(std::ostream &os, const Node *node)
{
    os << "Pos: (" << node->m_state.first.first << ", " << node->m_state.first.second << ")" << "t: " << node->m_state.second;
    return os;
}

Point &operator*=(Point &arg, float num)
{
    arg.first *= num;
    arg.second *= num;
    return arg;
}

Point operator*(const Point &arg, float num)
{
    Point res = arg;
    res *= num;
    return res;
}


Point operator*(float num, const Point &arg)
{
    return arg * num;
}


Point &operator/=(Point &arg, float num)
{
    arg *= (1 / num);
    return arg;
}


Point operator/(const Point &arg, float num)
{
    return arg * (1 / num);
}


Point operator/(float num, const Point &arg)
{
    return arg / num;
}


Point &operator +=(Point &arg1, const Point &arg2)
{
    arg1.first += arg2.first;
    arg1.second += arg2.second;
    return arg1;
}


Point operator +(const Point &arg1, const Point &arg2)
{
    Point res = arg1;
    res += arg2;
    return res;
}


Point &operator -=(Point &arg1, const Point &arg2)
{
    arg1.first -= arg2.first;
    arg1.second -= arg2.second;
    return arg1;
}


Point operator -(const Point &arg1, const Point &arg2)
{
    Point res = arg1;
    res -= arg2;
    return res;
}

Point normalizeVec(const Point &arg)
{
    return arg * (1.0 / sqrt(arg.first * arg.first + arg.second * arg.second));
}

bool operator ==(const Point &point1, const Point &point2)
{
    float tolerance = pow(10, -5);
    return fabs(point1.first - point2.first) < tolerance && fabs(point1.second - point2.second) < tolerance;
}

bool operator !=(const Point &point1, const Point &point2)
{
    return !(point1 == point2);
}


std::ostream &operator <<(std::ostream &os, const Point &point)
{
    os << "Pos: " << point.first << ", " << point.second;
    return os;
}
