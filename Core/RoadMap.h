#ifndef ROADMAP_H
#define ROADMAP_H

#include <vector>
#include <memory>
#include <Core/ConfigurationTimeGrid.h>

class Node;

class RoadMap
{
public:
    RoadMap();
    RoadMap(const std::pair<float, float> &sceneSize, size_t numNodes);

    /**
     * @brief plan
     * Do planning in state time space to find trajectories for all the robots
     * such that they move to their goals respectively, but do not collide with each other
     * The returned i-th path is for i-th mouse to i-th goal pos
     * Here we assume i-th mouse is going to the i-th goal and all the mice have the same
     * priorities. Variations could be i-th mouse goes to j-th goal and give the mice
     * different priorities.
     *
     * SUPER IMPORTANT:
     * Note that the nodes in paths are the nodes in state time grid, not the nodes
     * in the roadmap, this is especially important when we use the pointer to the
     * nodes as map keys. A key pointing to a node in roadmap is different from
     * key pointing to the node in state time grid which has the same position
     * with the node in the roadmap.
     */
    bool plan(std::vector<std::vector<Node *> > &paths) const;


    /**
     * @brief getFirstNNodes: get the first n nodes in DFS
     * @param n
     */
    void getFirstNNodes(size_t n, std::vector<Node *> &firstNnodes) const;
    Node *getRoot() const;
    unsigned int getNumNodes() const;

    std::vector<Node *> getNewMousePos();

    std::vector<Node *> getCurrMousePos() const;

    std::vector<Node *> getNewGoalPos();

    std::vector<Node *> getCurrGoalPos() const;

    void updateMousePos(const std::vector<Node *> &newPos);

private:
    ConfigurationTimeGrid m_stateTimeGrid;
    std::vector<std::shared_ptr<Node> > m_nodes;
    std::vector<Node *> m_mousePos;
    std::vector<Node *> m_goalPos;

    bool isCollideWithMouse(const Node *node) const;
    bool isGoalPos(const Node *node) const;

    void generateFreePos(size_t numPos, std::vector<Node *> &freePos) const;

    /**
     * @brief generateMap
     * @param sceneSize: a pair, first element is scene width, second element is scene height
     * @param num: number of nodes
     * -----------------------------------------> width(x)
     * |
     * |
     * |
     * |
     * |
     * |
     * |
     * \/
     * height(y)
     */
    void generateMap(const std::pair<float, float> &sceneSize, size_t num);
    void makeNeighbors(Node *node1, Node *node2) const;

    /**
     * @brief updateReservedNodes
     * Found a new path, reserve the nodes on this path in the state time grid
     */
    void updateReservedNodes(std::unordered_map<Edge, std::unordered_set<float> > &reserved, const std::vector<Node *> &path, std::unordered_map<Node *, float> &node2reservedStartingTime) const;

    /**
     * @brief outputPath
     * For debugging
     */
    void outputPath(const std::vector<Node *> &path) const;
};

#endif // ROADMAP_H
