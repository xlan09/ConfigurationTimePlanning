#ifndef CONFIGURATIONTIMEGRID_H
#define CONFIGURATIONTIMEGRID_H

#include <vector>
#include <memory>
#include <unordered_set>
#include <unordered_map>

class Node;
class RoadMap;

typedef std::pair<int, int> Edge;
typedef std::pair<float, float> Point;

class ConfigurationTimeGrid
{
public:
    ConfigurationTimeGrid();
    ConfigurationTimeGrid(Node *roadMapRoot, float velocity, float timeStep);

    /**
     * @brief searchPathByAstar
     * Search path in state time space
     */
    bool searchPathByAstar(Node *startNode,
                           Node *endNode,
                           std::vector<Node *> &path,
                           const std::unordered_map<Edge, std::unordered_set<float> > &reserved,
                           const std::unordered_map<Node *, float> &node2reservedStartingTime) const;

    /**
     * @brief getRoot
     * @return
     */
    Node *getRoot() const;

    float getTravelTimeBetweeNodes() const;

    float getVelocityBetweenNodes() const;

    size_t getMaxDiscretizedNodesInOneEdge() const;

private:
    float m_velocityBetweenNodes;
    float m_travelTimeBetweeNodes;
    size_t m_maxDiscretizedNodesInOneEdge;
    std::vector<std::shared_ptr<Node> > m_nodes;
    size_t createConfigurationTimeGrid(Node *roadMapRoot, float cellLength);
    void discretize(Node *prev, Node *curr, float cellLength, std::unordered_map<Point, std::unordered_set<Point> > &point2Neighbors) const;

    /**
     * @brief outputGrid: used for debugging
     */
    void outputGrid() const;

    void generateConsistentHeuristics(std::unordered_map<Node *, float> &heuristics, const Node *endNode) const;
};

#endif // CONFIGURATIONTIMEGRID_H
