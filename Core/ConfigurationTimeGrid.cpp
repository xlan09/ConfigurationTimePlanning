#include "ConfigurationTimeGrid.h"
#include "Core/RoadMap.h"
#include "Core/Node.h"
#include <math.h>
#include <stack>
#include <iostream>
#include "Core/SearchUtil.h"
#include <limits>
#include <algorithm>

typedef std::pair<float, float> Point;

ConfigurationTimeGrid::ConfigurationTimeGrid() : m_velocityBetweenNodes(0), m_travelTimeBetweeNodes(0), m_maxDiscretizedNodesInOneEdge(0)
{

}

ConfigurationTimeGrid::ConfigurationTimeGrid(Node *roadMapRoot, float velocity, float timeStep) : m_velocityBetweenNodes(velocity), m_travelTimeBetweeNodes(timeStep)
{
    m_maxDiscretizedNodesInOneEdge = createConfigurationTimeGrid(roadMapRoot, velocity * timeStep);
}

Node *ConfigurationTimeGrid::getRoot() const
{
    if(m_nodes.empty()) return nullptr;
    else return m_nodes[0].get();
}

float ConfigurationTimeGrid::getTravelTimeBetweeNodes() const
{
    return m_travelTimeBetweeNodes;
}

float ConfigurationTimeGrid::getVelocityBetweenNodes() const
{
    return m_velocityBetweenNodes;
}

size_t ConfigurationTimeGrid::getMaxDiscretizedNodesInOneEdge() const
{
    return m_maxDiscretizedNodesInOneEdge;
}

/**
 * @brief ConfigurationTimeGrid::createConfigurationTimeGrid
 * This will be called once to create the grid
 * Returns the largest number of grid nodes among all the edges of the roadmap
 */
size_t ConfigurationTimeGrid::createConfigurationTimeGrid(Node *roadMapRoot, float cellLength)
{
    if (roadMapRoot == nullptr) return 0;

    size_t maxDiscretizedNodesInOneEdge = 0;

    std::unordered_map<Point, std::unordered_set<Point>> point2Neighbors;

    std::stack<Node *> nodes;
    std::unordered_set<Node *> visited;
    nodes.push(roadMapRoot);
    while(!nodes.empty())
    {
        Node *curr = nodes.top();
        nodes.pop();
        if (visited.find(curr) == visited.end())
        {
            visited.insert(curr); // mark visited
            std::vector<Node *> neighbors = curr->neighbors();
            for(size_t i = 0; i < neighbors.size(); ++i)
            {
                if(visited.find(neighbors[i]) == visited.end()) // check this to make sure we only visited the undirected edge only once
                {
                    maxDiscretizedNodesInOneEdge = std::max(maxDiscretizedNodesInOneEdge,  (size_t)ceil(curr->dist2node(neighbors[i]) / cellLength) + 1);
                    discretize(curr, neighbors[i], cellLength, point2Neighbors);
                    nodes.push(neighbors[i]);
                }
            }
        }
    }

    // create grid
    std::unordered_map<Point, Node *> nodeCreated;
    for(std::unordered_map<Point, std::unordered_set<Point>>::const_iterator iter = point2Neighbors.begin(); iter != point2Neighbors.end(); ++iter)
    {
        if (nodeCreated.find(iter->first) == nodeCreated.end()) // a node may already be created since it may be other nodes' neighbor
        {
            std::shared_ptr<Node> newNode = std::shared_ptr<Node>(new Node(m_nodes.size(), iter->first));
            nodeCreated[iter->first] = newNode.get();
            m_nodes.push_back(newNode);
        }

        std::unordered_set<Point> pointNeighbors = iter->second;
        for(std::unordered_set<Point>::const_iterator neighborIter = pointNeighbors.begin(); neighborIter != pointNeighbors.end(); ++neighborIter)
        {
            if (nodeCreated.find(*neighborIter) == nodeCreated.end())
            {
                std::shared_ptr<Node> neighborNode = std::shared_ptr<Node>(new Node(m_nodes.size(), *neighborIter));
                nodeCreated[*neighborIter] = neighborNode.get();
                m_nodes.push_back(neighborNode);
            }

            nodeCreated[iter->first]->addNeighbor(nodeCreated[*neighborIter]);
        }
    }

    // output grid
//    outputGrid();

    return maxDiscretizedNodesInOneEdge;
}

/**
 * @brief ConfigurationTimeGrid::discretize
 * Discrete one edge
 * @param prev
 * @param curr
 * @param cellLength
 */
void ConfigurationTimeGrid::discretize(Node *prev, Node *curr, float cellLength, std::unordered_map<Point, std::unordered_set<Point>> &point2Neighbors) const
{
    if (prev == nullptr || curr == nullptr) return;

    Point prevGridPoint = prev->pos();
    if (point2Neighbors.find(prevGridPoint) == point2Neighbors.end())
    {
        point2Neighbors[prevGridPoint] = std::unordered_set<Point>();
    }

    point2Neighbors[prevGridPoint].insert(prevGridPoint); // at each grid point, the robot can stay

    // consider points between
    if(prev->dist2node(curr) > cellLength)
    {
        Point unitVec = normalizeVec(curr->pos() - prev->pos());
        for(int i = 0; i < floor(prev->dist2node(curr) / cellLength); ++i)
        {
            Point gridPoint = unitVec * cellLength * (i + 1) + prev->pos();
            if (point2Neighbors.find(gridPoint) == point2Neighbors.end())
            {
                point2Neighbors[gridPoint] = std::unordered_set<Point>();
            }

            point2Neighbors[gridPoint].insert(gridPoint); // at each grid point, the robot can stay
            point2Neighbors[gridPoint].insert(prevGridPoint); // neighbor with previous point
            point2Neighbors[prevGridPoint].insert(gridPoint);

            prevGridPoint = gridPoint;
        }
    }

    // add current node
    Point currGridPoint = curr->pos();
    if(currGridPoint != prevGridPoint)
    {
        if (point2Neighbors.find(currGridPoint) == point2Neighbors.end())
        {
            point2Neighbors[currGridPoint] = std::unordered_set<Point>();
        }

        point2Neighbors[currGridPoint].insert(currGridPoint); // at each grid point, the robot can stay
        point2Neighbors[currGridPoint].insert(prevGridPoint); // neighbor with previous point
        point2Neighbors[prevGridPoint].insert(currGridPoint);
    }
}

/**
 * @brief ConfigurationTimeGrid::outputGrid
 * Used for debugging
 */
void ConfigurationTimeGrid::outputGrid() const
{
    for(size_t i = 0; i < m_nodes.size(); ++i)
    {
        std::cout << m_nodes[i]->pos().first << ", " << m_nodes[i]->pos().second;
        std::vector<Node *> neighbors = m_nodes[i]->neighbors();
        for(size_t j = 0; j < neighbors.size(); ++j)
        {
            std::cout << ", nb: " << neighbors[j]->pos().first << ", " << neighbors[j]->pos().second;
        }

        std::cout << std::endl;
    }
}

void ConfigurationTimeGrid::generateConsistentHeuristics(std::unordered_map<Node *, float> &heuristics, const Node *endNode) const
{
    heuristics.clear();
    for(size_t i = 0; i < m_nodes.size(); ++i)
    {
        heuristics[m_nodes[i].get()] = m_nodes[i]->dist2node(endNode);
    }
}

bool ConfigurationTimeGrid::searchPathByAstar(Node *startNode,
                                              Node *endNode,
                                              std::vector<Node *> &path,
                                              const std::unordered_map<Edge, std::unordered_set<float> > &reserved,
                                              const std::unordered_map<Node *, float> &node2reservedStartingTime) const
{
    if (startNode == nullptr || endNode == nullptr) throw std::invalid_argument("StartNode and endNode can not be null when searching a path between them");

    std::unordered_map<Node *, float> heuriestics;
    generateConsistentHeuristics(heuriestics, endNode);

    /**
     * Map key is node, map value is a pair with first element being the previous
     * node leading to the key node, and the second element being the distance
     */
    std::unordered_map<Node *, std::pair<Node *, float> > dist;
    std::unordered_set<Node *> unVisited;

    for(size_t i = 0; i < m_nodes.size(); ++i)
    {
        float currDist = std::numeric_limits<float>::max();
        if (m_nodes[i]->pos() == startNode->pos())
        {
            startNode = m_nodes[i].get(); // Find the node in the state time space that has the same position with startNode, important, since we use Node * as keys of map
            currDist = 0 + heuriestics[startNode];
        }

        if (m_nodes[i]->pos() == endNode->pos())
        {
            endNode = m_nodes[i].get(); // Same here
        }

        dist[m_nodes[i].get()] = std::make_pair(nullptr, currDist);
        unVisited.insert(m_nodes[i].get());
    }

    Node *currNode = startNode;
    bool isPathFind = false;
    while(!unVisited.empty())
    {
        currNode = SearchUtil::findUnvisitedNodeWithShortestDist(dist, unVisited);
        if (currNode == nullptr)
        {
            isPathFind = false; // means there are no path found
            break;
        }

        if (currNode == endNode)
        {
            isPathFind = true;
            break;
        }

        unVisited.erase(currNode);

        // calculate time to current node
        std::vector<Node *> pathToCurrNode;
        SearchUtil::findPath(dist, currNode, pathToCurrNode);
        float timeToCurrNode = (pathToCurrNode.size() - 1) * m_travelTimeBetweeNodes;

        std::vector<Node *> neighbors = currNode->neighbors();
        for(size_t  i = 0; i < neighbors.size(); ++i)
        {
            // Slightly different from classical A*, here we do not check whether neighbor has been visited
            if (! SearchUtil::isReserved(currNode, neighbors[i], endNode, timeToCurrNode, timeToCurrNode + m_travelTimeBetweeNodes, reserved, node2reservedStartingTime))
            {
                float temp = dist[currNode].second - heuriestics[currNode] + currNode->dist2node(neighbors[i]);

                if (temp < dist[neighbors[i]].second - heuriestics[neighbors[i]])
                {
                    dist[neighbors[i]].first = currNode;
                    dist[neighbors[i]].second = temp + heuriestics[neighbors[i]];
                }
            }

        }
    }

    if (isPathFind == false) return false; // no  path find
    // update the path found
    path.clear();
    SearchUtil::findPath(dist, endNode, path);// last element in path is the startNode

    return true;
}

